<?php
namespace App\controllers\Admin;

use App\controllers\Controller;
use Illuminate\Database\Capsule\Manager as DB;

class AdminController extends Controller
{
    /**
     * Homepage
     */

     public function index($request, $response, $args)
     {
       // Authorization
       if($this->container->Auth->Adminauth()) {
             $session = $this->container['session'];
             $Token = isset($session['Token']);
             if($Token){
                 // echo "UserName: ".$session['username'];
                 // dd($session->getIterator());
                 // header("Content-Type: application/json; charset=utf-8");
                 // echo " Login Ok";
                 $data = [
                   'name' => $session['name'],
                   'lastname' => $session['lastname'],
                   'username' => $session['username'],
                   'img' => $session['img'],
                   'level' => $session['level'],
                   'email' => $session['email']
                 ];

                 return view('Admin.index', $data);
                 exit(); //enhance Slim performance
             }

       }

       return $response->withRedirect(getroute['login']);

     }

    public function logout($request, $response, $args)
    {
      // Kill Sesstion
      $session = $this->container['session'];
      unset($session['csrf']);
      $session::destroy();
      return $response->withRedirect(getroute['Index']);
    }

}
