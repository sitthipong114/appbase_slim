<?php
namespace App\controllers\Users;

use App\Models\Users;
use Firebase\JWT\JWT;
use App\controllers\Controller;
use Illuminate\Database\Capsule\Manager as DB;

class RegisterController extends Controller
{
    /**
     * Homepage
     */

     public function index($request, $response, $args)
     {

       if($this->container->Auth->validateAuth()){
         // Check User Authen
         return $response->withRedirect('/users/index');
       }

       $data = [
         'captcha' => $this->HTMLRecaptcha(),
       ];

      return view('Users.register', $data);
     }

    public function checkregister($request, $response, $args){
      $name     = get($request, 'name');
      $lastname = get($request, 'lastname');
      $username = get($request, 'username');
      $password = get($request, 'password');
      $password_comfirm = get($request, 'password_comfirm');
      $Recaptcha = get($request, 'g-recaptcha-response');

      // echo $username.'<br>';
      // echo $password.'<br>';
      // echo $password_comfirm.'<br>';

      $datause = Users::checklogin($username);

      if (empty($datause) && $password == $password_comfirm && $this->Recaptcha($Recaptcha)) {

          $settings = $this->container['settings']; // get settings array.
          $token = JWT::encode(['username' => $username, 'password' => $password], $settings['jwt']['secret'], "HS256");
          // dd($this->container['session']);
          $session = $this->container['session'];
          $session['Token']    = $token;
          $session['id']       = RegisterController::Genid();
          $session['name']     = $name;
          $session['lastname'] = $lastname;
          $session['username'] = $username;
          $session['email']    = "NULL";
          $session['img']      = "OneTech/images/Users.png";
          $session['level']    = 1;

          DB::table('Users')->insert(
              [
                'id' => $session['id'],
                'name' => $session['name'],
                'lastname' => $session['lastname'],
                'username' => $session['username'],
                'password' => hashstring($password),
                'email' => $session['email'],
                'img' => $session['img'],
                'level' => $session['level'],
                'Active' => '1',
                'created_at' => date("Y-m-d H:i:s")
             ]
          );
          // dd($session->getIterator());
          return $response->withRedirect('/users/index');
          exit();
      }

      // บัญชีถูกปิดการใช้งาน
      if(!empty($datause) == 1 && $datause->active == 0){
        return $response->withJson(['error' => true, 'message' => 'Account not Active']);
        exit();
      }

      // recaptcha ไม่ถูกต้อง
      if(!$this->Recaptcha($Recaptcha)){
        return $response->withJson(['error' => true, 'message' => 'Recaptcha not match']);
        exit();
      }

      return $response->withJson(['error' => true, 'message' => 'These user or password do not match our records.']);
      exit();
    }

    Private Function Genid(){

      $Tmpyear = date("Y") - 2000;

      if(date("n") < 10){
        $Tmpmonth = "0".date("n");
      }else {
        $Tmpmonth = date("n");
      }

      if(date("d") < 10){
        $Tmpday = "0".date("d");
      }else {
        $Tmpday = date("d");
      }

      $Tmpsearch = $Tmpyear.$Tmpmonth;
      $Genbill  = DB::SELECT("SELECT IFNULL(MAX(CAST(id as CHAR)), 0) + 1 as Autoid FROM Users WHERE LEFT(id, 4) = ? ", [$Tmpsearch]);

      if($Genbill['0']->Autoid > 0){
        $Sautoid = $Genbill['0']->Autoid;
      }else {
        $Sautoid = "1";
      }

      if($Sautoid == 1){
        $Sautoid = $Tmpsearch."00000001";
      }else {
        $Sautoid = $Sautoid;
      }

      return $Sautoid;
    }


}
