<?php

use Slim\App;
use Slim\Http\Request;
use Slim\Http\Response;
use Illuminate\Database\Capsule\Manager as DB;


return function (App $app) {
    $container = $app->getContainer();

    // index page
    $app->get('/', \App\Controllers\IndexController::class . ':Index');
    $app->get('/index', \App\Controllers\IndexController::class . ':Index')->setName('Index');
    $app->get('/login',function(Request $request, Response $response, array $args) use($container) {
      echo "<a href='index'>index</a><br/>";
      echo "<a href='users/login'>Users</a><br/>";
      echo "<a href='#'>Admin</a><br/>";
      echo "<a href='404'>Error404 page</a><br/>";
    });

   /**
    * Users Group UsersController
    */
    $app->group('/users', function() use($app, $container){

        // Users Login Page
        $app->get('', \App\Controllers\Users\UsersController::class . ':login');
        $app->get('/login', \App\Controllers\Users\UsersController::class . ':login')->setName('login');

        // User Check login
        $app->post('/checklogin', \App\Controllers\Users\UsersController::class . ':checklogin')->setName('checklogin');

        // Index Page Users
        $app->get('/index', \App\Controllers\Users\UsersController::class . ':index');

        // Users logout page
        $app->get('/logout', \App\Controllers\Users\UsersController::class . ':logout')->setName('logout');

        // Register logout page
        $app->get('/register', \App\Controllers\Users\RegisterController::class . ':index')->setName('register');

        // Check Register page
        $app->post('/checkregister', \App\Controllers\Users\RegisterController::class . ':checkregister')->setName('checkregister');

    });


    $app->group('/admin', function($response) use($app, $container){

        // Index Page Users
        $app->get('/index', \App\Controllers\Admin\AdminController::class . ':index');

        // Users logout page
        $app->get('/logout', \App\Controllers\Admin\AdminController::class . ':logout')->setName('logout');

    });

    /**
     * Users Group APIController
     */
    $app->group('/api', function(\Slim\App $app) use($container) {

    });

    //******************************* ERROR PAGE *******************************//

    $app->get('/404', function(Request $request, Response $response, array $args) use($container){
          return view('Error.404');
     });


     setroute($container->router->getRoutes(), 'getroute');
};
