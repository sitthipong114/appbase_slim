﻿<!DOCTYPE html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Login | Kiaalap - Kiaalap Admin Template</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Google Fonts
		============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Play:400,700" rel="stylesheet">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset('AdminLTE/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset('kiaalap/style.css') }}">

    <style>
    * {
      box-sizing: border-box;
    }

    .container {
      max-width: 300px;
      background: #ccc;
      padding: 20px;
    }

    .g-recaptcha {
      transform-origin: left top;
      -webkit-transform-origin: left top;
    }
</style>

</head>

<body>
    <!--[if lt IE 8]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->
	<div class="error-pagewrap">
		<div class="error-page-int">
			<div class="text-center m-b-md custom-login">
				<h3>สมัครสมาชิก</h3>
        <p><a href="/index">กลับหน้าแรก</a></p>
			</div>
			<div class="content-error">
				<div class="hpanel">
                    <div class="panel-body">
                        <form action="checkregister" id="loginForm" method="post">
                            <div class="form-group">
                                {!! csrf !!}
                                <label class="control-label" for="username">ชื่อจริง</label>
                                <input type="text" title="Please enter you username" required="" value="" name="name" id="username" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="username">นามสกุล</label>
                                <input type="text" title="Please enter you username" required="" value="" name="lastname" id="username" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="username">ชื่อผู้ใช้งาน</label>
                                <input type="text" placeholder="username (0-9,a-z)" title="Please enter you username" required="" value="" name="username" id="username" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="password">รหัสผ่าน</label>
                                <input type="password" title="ใส่รหัสผ่านของคุณ" placeholder="รหัสผ่าน" required="" value="" name="password" id="password" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="password_comfirm">ยืนยันรหัสผ่าน</label>
                                <input type="password" title="ยืนยันรหัสผ่านของคุณอีกครั้ง" placeholder="กรุณายืนยันรหัสผ่านอีกครั้ง" required="" value="" name="password_comfirm" id="password_comfirm" class="form-control">
                            </div>

                            <div class="form-group"> {!! $captcha !!} </div>
                            <button class="btn btn-success btn-block loginbtn">สร้างบัญชี</button>
                            <input  class="btn btn-default btn-block" type="button" onclick="location.href='login'" value="เข้าสู่ระบบ"/>
                        </form>

                    </div>
                </div>
			</div>
			<div class="text-center login-footer">
				<p>Copyright © 2018. All rights reserved. by <a href="https://github.com/STP5940/appbase_slim">Appbase slim</a></p>
			</div>
		</div>
    </div>
    <!-- jquery
		============================================ -->
    <script src="{{ asset('AdminLTE/bower_components/jquery/dist/jquery.min.js') }}"></script>
    <!-- icheck JS
		============================================ -->
    <script src="{{ asset('AdminLTE/plugins/iCheck/icheck.min.js') }}"></script>

    <script>
        (function ($) {
         "use strict";

          $('.i-checks').iCheck({
        		checkboxClass: 'icheckbox_square-green',
        		radioClass: 'iradio_square-green',
        	});



        })(jQuery);

        $(function(){
          function rescaleCaptcha(){
            var width = $('.g-recaptcha').parent().width();
            var scale;
            if (width < 302) {
              scale = width / 302;
            } else{
              scale = 1.0;
            }

            $('.g-recaptcha').css('transform', 'scale(' + scale + ')');
            $('.g-recaptcha').css('-webkit-transform', 'scale(' + scale + ')');
            $('.g-recaptcha').css('transform-origin', '0 0');
            $('.g-recaptcha').css('-webkit-transform-origin', '0 0');
          }

          rescaleCaptcha();
          $( window ).resize(function() { rescaleCaptcha(); });

        });

    </script>
</body>

</html>
