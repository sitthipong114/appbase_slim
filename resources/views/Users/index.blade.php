<!DOCTYPE html>
<html lang="en">
<head>
<title>Cart</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="OneTech shop project">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="{{ asset('OneTech/styles/bootstrap4/bootstrap.min.css') }}">
<link href="{{ asset('OneTech/plugins/fontawesome-free-5.0.1/css/fontawesome-all.css') }}" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="{{ asset('OneTech/styles/cart_styles.css') }}">
{{-- <link rel="stylesheet" type="text/css" href="{{ asset('OneTech/styles/blog_single_styles.css') }}"> --}}
<link rel="stylesheet" type="text/css" href="{{ asset('OneTech/styles/cart_responsive.css') }}">

</head>

<body>

<div class="super_container">

	<!-- Header -->

	<header class="header">

		<!-- Top Bar -->

		<div class="top_bar">
			<div class="container">
				<div class="row">
					<div class="col d-flex flex-row">
						<div class="top_bar_content ml-auto">

							<div class="top_bar_user">
								<a href="#"><img class="rounded-circle" src="{{ asset($img) }}" alt="เข้าสู่ระบบ" onerror="this.src='{{ asset('OneTech/images/user.svg') }}'" width="40" height="35">
								<div>{{ $name }} {{ $lastname }}</div></a>
								<div data-toggle="modal" data-target="#modal-logout"><a href="#">ออกจากระบบ</a></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Header Main -->

		<div class="header_main">
			<div class="container">
				<div class="row">

					<!-- Logo -->
					<div class="col-lg-1 col-sm-2 col-3 order-1">
						<div class="logo_container">
							<img width= "88px" alt="Responsive image" src="{{ asset('OneTech/images/logo.png') }}">
						</div>
					</div>
					<div class="col-lg-2 col-sm-3 col-3 order-1">
						<div class="logo_container">
							<div class="logo"><a href="index">OneTech</a></div>
						</div>
					</div>

					<!-- Search -->
					<div class="col-lg-5 col-12 order-lg-2 order-3 text-lg-left text-right">
					</div>

					<!-- Wishlist -->
					<div class="col-lg-4 col-12 order-lg-3 order-2 text-lg-left text-right">
						<div class="wishlist_cart d-flex flex-row align-items-center justify-content-end">
							<div class="wishlist d-flex flex-row align-items-center justify-content-end">
								<div class="wishlist_icon"><img src="{{ asset('OneTech/images/Wallet.png') }}" alt=""></div>
								<div class="wishlist_content">
									<div class="wishlist_text"><a href="#">e-wallet</a></div>
									<div class="wishlist_count">500.00 ฿</div>
								</div>
							</div>

							<!-- Cart -->
							<div class="cart">
								<div class="cart_container d-flex flex-row align-items-center justify-content-end">
									<div class="cart_icon">
										<img src="{{ asset('OneTech/images/heart.png') }}" alt="">
										<div class="cart_count"><span>{{ $level }}</span></div>
									</div>
									<div class="cart_content">
										<div class="cart_text"><a href="#">level {{ $level }}</a></div>
										<div class="cart_price">สมาชิกทั่วไป</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Main Navigation -->

		<nav class="main_nav">
			<div class="container">
				<div class="row">
					<div class="col">

						<div class="main_nav_content d-flex flex-row">

							<!-- Categories Menu -->

							<div class="cat_menu_container">
								<div class="cat_menu_title d-flex flex-row align-items-center justify-content-start">
									<div class="cat_burger"><span></span><span></span><span></span></div>
									<div class="cat_menu_text">ข่าวเพิ่มเติม</div>
								</div>

								<ul class="cat_menu">
									<li><a href="#">แนะนำเพื่อนรับเงินฟรี 10% <i class="fas fa-chevron-right ml-auto"></i></a></li>
									<li><a href="#">วิธีฝากเงินเข้าสู่ระบบ <i class="fas fa-chevron-right ml-auto"></i></a></li>
								</ul>
							</div>

							<!-- Main Nav Menu -->

							<div class="main_nav_menu ml-auto">
								<ul class="standard_dropdown main_nav_dropdown">

									<i class="fa fa-trophy" aria-hidden="true"></i>
									<li class="hassubs">
										<a href="#">ตรวจสอบผลสลาก<i class="fas fa-chevron-down"></i></a>
										<ul>
											<li><a href="#">หวยฐัฐบาลไทย<i class="fas fa-chevron-down"></i></a></li>
											<li><a href="#">หวยรายวัน<i class="fas fa-chevron-down"></i></a></li>
										</ul>
									</li>

									<i class="fas fa-plus-square" aria-hidden="true"></i>
									<li><a href="#"> ฝากเงิน<i class="fas fa-chevron-down"></i></a></li>
									<i class="fas fa-minus-square" aria-hidden="true"></i>
									<li><a href="#"> ถอนเงิน<i class="fas fa-chevron-down"></i></a></li>
									<i class="fa fa-share-alt" aria-hidden="true"></i>
									<li><a href="#"> แนะนำเพื่อน<i class="fas fa-chevron-down"></i></a></li>
									<i class="fa fa-cogs" aria-hidden="true"></i>
									<li><a href="#"> ตั้งค่าบัญชี<i class="fas fa-chevron-down"></i></a></li>
									<i class="far fa-address-card" aria-hidden="true"></i>
									<li><a href="#"> ติดต่อแอดมิน<i class="fas fa-chevron-down"></i></a></li>
								</ul>
							</div>

							<!-- Menu Trigger -->

							<div class="menu_trigger_container ml-auto">
								<div class="menu_trigger d-flex flex-row align-items-center justify-content-end">
									<div class="menu_burger">
										<div class="menu_trigger_text">เมนู</div>
										<div class="cat_burger menu_burger_inner"><span></span><span></span><span></span></div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</nav>

		<!-- Menu -->

		<div class="page_menu">
			<div class="container">
				<div class="row">
					<div class="col">

						<div class="page_menu_content">

							<ul class="page_menu_nav">


								<li class="page_menu_item">
									<a href="#">
										<img class="rounded-circle" src="{{ asset($img) }}" alt="เข้าสู่ระบบ" onerror="this.src='{{ asset('OneTech/images/user.svg') }}'" width="40" height="35">
										&nbsp;{{ $name }} {{ $lastname }}<i class="fa fa-angle-down">
										</i>
									</a>
								</li>

								<li class="page_menu_item has-children">
									<a href="#"> ตรวจสอบผลสลาก<i class="fa fa-angle-down"></i></a>
									<ul class="page_menu_selection">
										<li><a href="#">หวยรัฐบาลไทย<i class="fa fa-angle-down"></i></a></li>
										<li><a href="#">หวยรายวัน<i class="fa fa-angle-down"></i></a></li>
									</ul>
								</li>
								<li class="page_menu_item"><a href="#"> ฝากเงิน<i class="fa fa-angle-down"></i></a></li>
								<li class="page_menu_item"><a href="#"> ถอนเงิน<i class="fa fa-angle-down"></i></a></li>
								<li class="page_menu_item"><a href="#"> แนะนำเพื่อน<i class="fa fa-angle-down"></i></a></li>
								<li class="page_menu_item"><a href="#"> ติดต่อแอดมิน<i class="fa fa-angle-down"></i></a></li>
								<li class="page_menu_item"><a data-toggle="modal" data-target="#modal-logout" href="#"> ออกจากระบบ<i class="fa fa-angle-down"></i></a></li>
							</ul>

						</div>
					</div>
				</div>
			</div>
		</div>

	</header>

	<!-- Blog Posts -->

	<div class="blog">
		<div class="container">
			<div class="row">
				<div class="col-lg-10 offset-lg-1">
					<div class="blog_posts d-flex flex-row align-items-start justify-content-between">

						<!-- Blog post -->
						<div class="blog_post col-6 col-sm-6 col-md-5 col-lg-4">
							<a href="#">
								<div class="blog_image" style="background-image:url({{ asset('OneTech/images/หวยออนไลน์.jpg') }})"></div>
							</a>
							<div class="ribbon ribbon-top-left"><span>HOT</span></div>
							<div class="blog_text">หวยรัฐบาลไทย</div>
							<div class="blog_button"><a href="#">อ่านต่อไป</a></div>
						</div>

						<!-- Blog post -->
						<div class="blog_post col-6 col-sm-6 col-md-5 col-lg-4">
							<a href="#">
								<div class="blog_image" style="background-image:url({{ asset('OneTech/images/จับยี่กี.jpg') }})"></div>
							</a>
							<div class="blog_text">หวยรายวัน (จับยี่กี VIP)</div>
							<div class="blog_button"><a href="#">อ่านต่อไป</a></div>
						</div>

						<!-- Blog post -->
						<div class="blog_post col-6 col-sm-6 col-md-5 col-lg-4">
							<a href="#">
								<div class="blog_image" style="background-image:url({{ asset('OneTech/images/game_coin.jpg') }})"></div>
							</a>
							<div class="blog_text">เป่ายิงฉุบ & หัวก้อย</div>
							<div class="blog_button"><a href="#">อ่านต่อไป</a></div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Cart -->

	<div class="cart_section">
		<div class="container">
			<div class="row">
				<div class="col-lg-10 offset-lg-1">
					<div class="cart_container">
						<div class="cart_title">Shopping Cart</div>

						<div class="cart_items">
							<ul class="cart_list">
								<li class="cart_item clearfix">
									<div class="cart_item_image"><img src="{{ asset('OneTech/images/shopping_cart.jpg') }}" alt=""></div>
									<div class="cart_item_info d-flex flex-md-row flex-column justify-content-between">
										<div class="cart_item_name cart_info_col">
											<div class="cart_item_title">Name</div>
											<div class="cart_item_text">MacBook Air 13</div>
										</div>
										<div class="cart_item_color cart_info_col">
											<div class="cart_item_title">Color</div>
											<div class="cart_item_text"><span style="background-color:#999999;"></span>Silver</div>
										</div>
										<div class="cart_item_quantity cart_info_col">
											<div class="cart_item_title">Quantity</div>
											<div class="cart_item_text">1</div>
										</div>
										<div class="cart_item_price cart_info_col">
											<div class="cart_item_title">Price</div>
											<div class="cart_item_text">$2000</div>
										</div>
										<div class="cart_item_total cart_info_col">
											<div class="cart_item_title">Total</div>
											<div class="cart_item_text">$2000</div>
										</div>
									</div>
								</li>
							</ul>
						</div>

						<!-- Order Total -->
						<div class="order_total">
							<div class="order_total_content text-md-right">
								<div class="order_total_title">Order Total:</div>
								<div class="order_total_amount">$2000</div>
							</div>
						</div>

						<div class="cart_buttons">
							<button type="button" class="button cart_button_clear">Add to Cart</button>
							<button type="button" class="button cart_button_checkout">Add to Cart</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Newsletter -->

	<div class="newsletter">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="newsletter_container d-flex flex-lg-row flex-column align-items-lg-center align-items-center justify-content-lg-start justify-content-center">
						<div class="newsletter_title_container">
							<div class="newsletter_icon"><img src="{{ asset('OneTech/images/send.png') }}" alt=""></div>
							<div class="newsletter_title">Sign up for Newsletter</div>
							<div class="newsletter_text"><p>...and receive %20 coupon for first shopping.</p></div>
						</div>
						<div class="newsletter_content clearfix">
							<form action="#" class="newsletter_form">
								<input type="email" class="newsletter_input" required="required" placeholder="Enter your email address">
								<button class="newsletter_button">Subscribe</button>
							</form>
							<div class="newsletter_unsubscribe_link"><a href="#">unsubscribe</a></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Footer -->

	<div class="custom_dropdown col-lg-0 col-0">
		<div class="custom_dropdown_list">
			<span class="custom_dropdown_placeholder clc"></span>
			<ul class="custom_list clc">
			</ul>
		</div>
	</div>

	<footer class="footer">
		<div class="container">
			<div class="row">

				<div class="col-lg-3 footer_col">
					<div class="footer_column footer_contact">
						<div class="logo_container">
							<div class="logo"><a href="#">OneTech</a></div>
						</div>
						<div class="footer_title">Got Question? Call Us 24/7</div>
						<div class="footer_phone">+38 068 005 3570</div>
						<div class="footer_contact_text">
							<p>17 Princess Road, London</p>
							<p>Grester London NW18JR, UK</p>
						</div>
						<div class="footer_social">
							<ul>
								<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
								<li><a href="#"><i class="fab fa-twitter"></i></a></li>
								<li><a href="#"><i class="fab fa-youtube"></i></a></li>
								<li><a href="#"><i class="fab fa-google"></i></a></li>
								<li><a href="#"><i class="fab fa-vimeo-v"></i></a></li>
							</ul>
						</div>
					</div>
				</div>

				<div class="col-lg-2 offset-lg-2">
					<div class="footer_column">
						<div class="footer_title">Find it Fast</div>
						<ul class="footer_list">
							<li><a href="#">Computers & Laptops</a></li>
							<li><a href="#">Cameras & Photos</a></li>
							<li><a href="#">Hardware</a></li>
							<li><a href="#">Smartphones & Tablets</a></li>
							<li><a href="#">TV & Audio</a></li>
						</ul>
						<div class="footer_subtitle">Gadgets</div>
						<ul class="footer_list">
							<li><a href="#">Car Electronics</a></li>
						</ul>
					</div>
				</div>

				<div class="col-lg-2">
					<div class="footer_column">
						<ul class="footer_list footer_list_2">
							<li><a href="#">Video Games & Consoles</a></li>
							<li><a href="#">Accessories</a></li>
							<li><a href="#">Cameras & Photos</a></li>
							<li><a href="#">Hardware</a></li>
							<li><a href="#">Computers & Laptops</a></li>
						</ul>
					</div>
				</div>

				<div class="col-lg-2">
					<div class="footer_column">
						<div class="footer_title">Customer Care</div>
						<ul class="footer_list">
							<li><a href="#">My Account</a></li>
							<li><a href="#">Order Tracking</a></li>
							<li><a href="#">Wish List</a></li>
							<li><a href="#">Customer Services</a></li>
							<li><a href="#">Returns / Exchange</a></li>
							<li><a href="#">FAQs</a></li>
							<li><a href="#">Product Support</a></li>
						</ul>
					</div>
				</div>

			</div>
		</div>
	</footer>

	<!--  modal logout confirm -->
	<div id="modal-logout" class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLongTitle">แจ้งเตือน</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">คุณต้องการออกจากระบบ</div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
	        <a href="/users/logout"><button type="button" class="btn btn-primary">ยืนยัน</button></a>
	      </div>
	    </div>
	  </div>
	</div>

	<!-- Copyright -->

	<div class="copyright">
		<div class="container">
			<div class="row">
				<div class="col">

					<div class="copyright_container d-flex flex-sm-row flex-column align-items-center justify-content-start">
						<div class="copyright_content"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
</div>
						<div class="logos ml-sm-auto">
							<ul class="logos_list">
								<li><a href="#"><img src="{{ asset('OneTech/images/logos_1.png') }}" alt=""></a></li>
								<li><a href="#"><img src="{{ asset('OneTech/images/logos_2.png') }}" alt=""></a></li>
								<li><a href="#"><img src="{{ asset('OneTech/images/logos_3.png') }}" alt=""></a></li>
								<li><a href="#"><img src="{{ asset('OneTech/images/logos_4.png') }}" alt=""></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="{{ asset('OneTech/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('OneTech/styles/bootstrap4/popper.js') }}"></script>
<script src="{{ asset('OneTech/styles/bootstrap4/bootstrap.min.js') }}"></script>
<script src="{{ asset('OneTech/plugins/greensock/TweenMax.min.js') }}"></script>
<script src="{{ asset('OneTech/plugins/greensock/TimelineMax.min.js') }}"></script>
<script src="{{ asset('OneTech/plugins/scrollmagic/ScrollMagic.min.js') }}"></script>
<script src="{{ asset('OneTech/plugins/greensock/animation.gsap.min.js') }}"></script>
<script src="{{ asset('OneTech/plugins/greensock/ScrollToPlugin.min.js') }}"></script>
<script src="{{ asset('OneTech/plugins/easing/easing.js') }}"></script>
<script src="{{ asset('OneTech/js/cart_custom.js') }}"></script>
</body>

</html>
